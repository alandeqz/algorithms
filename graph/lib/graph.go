package lib

import (
	"errors"
	"sync"
)

var (
	ErrEmptyGraph    = errors.New("empty graph provided!")
	ErrDeletionError = errors.New("node deletion error!")
)

// Graph the structure of the whole graph.
type Graph struct {
	Nodes []interface{}                 // the list of nodes in a graph
	Edges map[interface{}][]interface{} // the list of edges of the nodes of a graph
	Size  int                           // the size of the graph (# of nodes)
	Lock  sync.RWMutex                  // locking mutex to avoid multiple writing to map
}

// AddNode adds the node to the graph.
func (g *Graph) AddNode(node interface{}) {
	g.Lock.Lock()

	g.Nodes = append(g.Nodes, node)

	g.Size++

	g.Lock.Unlock()
}

// AddEdge adds an edge to the graph as a connection between two nodes.
func (g *Graph) AddEdge(n1, n2 interface{}) {
	g.Lock.Lock()

	if g.Edges == nil {
		g.Edges = make(map[interface{}][]interface{}, 0)
	}

	firstNodeExists, secondNodeExists := false, false

	for _, item := range g.Nodes {
		if item == n1 {
			firstNodeExists = true
		}
		if item == n2 {
			secondNodeExists = true
		}
	}

	if firstNodeExists {
		g.Size++
	}

	if secondNodeExists {
		g.Size++
	}

	g.Edges[n1] = append(g.Edges[n1], n2)

	g.Lock.Unlock()
}

// DeleteNode deletes a node with the corresponding edges from the graph.
func (g *Graph) DeleteNode(n interface{}) error {
	if g == nil {
		return ErrEmptyGraph
	}

	initialSize := g.Size
	g.Lock.Lock()
	for index, node := range g.Nodes {
		if node == n {
			right := g.Nodes[index+1:]
			g.Nodes = g.Nodes[:index]
			g.Nodes = append(g.Nodes, right...)
			g.Size--
		}
	}
	delete(g.Edges, n)
	g.Lock.Unlock()

	if g.Size == initialSize {
		return ErrDeletionError
	}

	return nil
}
