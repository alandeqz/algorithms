package dfs

import (
	"fmt"
	"gitlab.com/alandeqz/algorithms/graph/lib"
)

var visited = make(map[interface{}]bool) // the map containing the already visited nodes

// DFS makes a traversal through the graph by using the
// depth-first search algorithm.
func DFS(graph *lib.Graph) error {
	// checking if the graph is not empty:
	if graph == nil || graph.Size == 0 {
		return lib.ErrEmptyGraph
	}

	// starting with the first node in the graph:
	currentNode := graph.Nodes[0]

	// calling a recursive function to traverse the graph:
	return dfsRecursive(graph, currentNode)
}

// dfsRecursive makes a recursive traversal through the graph.
func dfsRecursive(graph *lib.Graph, currentNode interface{}) error {
	// marking the current node as visited:
	visited[currentNode] = true

	// printing the current node:
	fmt.Println("NODE:", currentNode)

	// looping through the adjacent nodes of the current node:
	for _, item := range graph.Edges[currentNode] {
		// checking if the adjacent node has not been visited yet, otherwise skip the iteration:
		if !visited[item] {
			// continuing the recursion with the current node:
			_ = dfsRecursive(graph, item)
		}
	}

	// exiting the method without any errors:
	return nil
}
