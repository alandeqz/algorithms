package bfs

import (
	"fmt"
	"gitlab.com/alandeqz/algorithms/graph/lib"
)

// BFS makes a traversal through the graph by using the
// breadth-first search algorithm.
func BFS(graph *lib.Graph) error {
	// checking if the graph is not empty:
	if graph == nil || graph.Size == 0 {
		return lib.ErrEmptyGraph
	}

	// initializing the queue slice:
	queue := make([]interface{}, 0, graph.Size)

	// initializing the map to store the visited nodes:
	visited := make(map[interface{}]bool)

	// adding the first node to the queue:
	queue = append(queue, graph.Nodes[0])

	// marking the first node as visited:
	visited[graph.Nodes[0]] = true

	// printing the first node:
	fmt.Println("NODE:", graph.Nodes[0])

	// iterating through the queue, while it is not empty:
	for len(queue) != 0 {
		// getting the front node from the queue:
		currentNode := queue[0]

		// deleting the front node from the queue:
		queue = queue[1:]

		// iterating through the edges of the current node:
		for _, item := range graph.Edges[currentNode] {
			// checking if the adjacent node has not been visited yet, otherwise skip the iteration:
			if !visited[item] {
				// printing the current node:
				fmt.Println("NODE:", item)

				// marking the current node as visited:
				visited[item] = true

				// adding the current node to the queue:
				queue = append(queue, item)
			}
		}
	}

	// exiting the method without any errors:
	return nil
}
