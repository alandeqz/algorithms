package main

import (
	"fmt"
	"gitlab.com/alandeqz/algorithms/graph/bfs"
	"gitlab.com/alandeqz/algorithms/graph/dfs"
	"gitlab.com/alandeqz/algorithms/graph/lib"
	"log"
	"sync"
)

func main() {
	g := &lib.Graph{}

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 1; i <= 10; i++ {
			g.AddNode(i)
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		g.AddEdge(1, 2)
		g.AddEdge(2, 3)
		g.AddEdge(3, 4)
		g.AddEdge(4, 5)
		g.AddEdge(5, 6)
		g.AddEdge(6, 7)
		g.AddEdge(7, 8)
		g.AddEdge(8, 9)
		g.AddEdge(9, 10)
		g.AddEdge(10, 1)
	}()

	wg.Wait()

	fmt.Println("\n\ngraph traversal using BFS:")

	err := bfs.BFS(g)
	if err != nil {
		log.Fatal("BFS error:", err)
		return
	}

	fmt.Println("\ngraph traversal using DFS:")

	err = dfs.DFS(g)
	if err != nil {
		log.Fatal("DFS error:", err)
		return
	}
}
