package main

import (
	"fmt"
	"gitlab.com/alandeqz/algorithms/sorting/bubbleSort"
	"gitlab.com/alandeqz/algorithms/sorting/insertionSort"
	"gitlab.com/alandeqz/algorithms/sorting/mergeSort"
)

func main() {
	array := []int{10, 5, 6, 3, 4, 2, 7, 8, 1, 9}

	fmt.Println("\ninitial array:", array)

	fmt.Println("\narray after merge sort:", mergeSort.MergeSort(array))

	array = []int{10, 5, 6, 3, 4, 2, 7, 8, 1, 9}

	fmt.Println("\narray after insertion sort:", insertionSort.InsertionSort(array))

	array = []int{10, 5, 6, 3, 4, 2, 7, 8, 1, 9}

	fmt.Println("\narray after bubble sort:", bubbleSort.BubbleSort(array))
}
