package mergeSort

// merge merges the two splitted arrays.
func merge(left []int, right []int) []int {
	// resultant array:
	result := make([]int, 0)

	// looping through the left and right splitted arrays:
	for len(left) > 0 && len(right) > 0 {
		// checking if the current element of the left array is less than the current element of the right array:
		if left[0] < right[0] {
			// adding the minimal element to the resultant array:
			result = append(result, left[0])
			// dropping the current element from the left array:
			left = left[1:]
		} else {
			// adding the minimal element to the resultant array:
			result = append(result, right[0])
			// dropping the current element from the right array:
			right = right[1:]
		}
	}

	// adding the remaining elements of the left array to the result:
	for _, item := range left {
		result = append(result, item)
	}

	// adding the remaining elements of the right array to the result:
	for _, item := range right {
		result = append(result, item)
	}

	// returning the result:
	return result
}

// MergeSort splits the array in two equal parts.
func MergeSort(arr []int) []int {
	// checking if the array is non-empty:
	if len(arr) <= 1 {
		return arr
	}

	// creating the left part of an array:
	leftArr := arr[:len(arr)/2]

	// creating the right part of an array:
	rightArr := arr[len(arr)/2:]

	// recursively calling the merging method based on two splitted arrays:
	return merge(MergeSort(leftArr), MergeSort(rightArr))
}
