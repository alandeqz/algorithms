package insertionSort

// InsertionSort sorts an array by the insertion sort algorithm.
func InsertionSort(array []int) []int {
	// looping through the array:
	for i := 0; i < len(array); i++ {
		// getting the current element and store it in the "key" variable:
		key := array[i]

		// introducing the second index to the left:
		j := i - 1

		// going left of the current element, while it is not in the proper place:
		for j >= 0 && array[j] > key {
			// moving the element occupying the target destination to the right:
			array[j+1] = array[j]

			// decrementing the index:
			j--
		}

		// placing the key in the proper position:
		array[j+1] = key
	}

	// returning the sorted array:
	return array
}
