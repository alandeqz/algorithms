package bubbleSort

// BubbleSort sorts the given array by the bubble sort algorithm.
func BubbleSort(array []int) []int {
	// iterating through the array:
	for i := 0; i < len(array); i++ {
		// the second loop to swap the needed elements:
		for j := 0; j < len(array)-1-i; j++ {
			// checking if swapping is necessary:
			if array[j] > array[j+1] {
				// swapping algorithm:
				temp := array[j]
				array[j] = array[j+1]
				array[j+1] = temp
			}
		}
	}

	// returning the sorted array:
	return array
}
