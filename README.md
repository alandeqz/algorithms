# Algorithms

Implementation of various algorithms in [GoLang](https://golang.org/).

The readme file will be updated as the new algorithms are added.

Author: [Alan Seksenali](https://www.linkedin.com/in/alanseksenali/)

## Contents

### Graph

- [Depth-First Search (DFS)](https://gitlab.com/alandeqz/algorithms/-/tree/master/graph/dfs)
- [Breadth-First Search (BFS)](https://gitlab.com/alandeqz/algorithms/-/tree/master/graph/bfs)

### Sorting

- [Merge Sort](https://gitlab.com/alandeqz/algorithms/-/tree/master/sorting/mergeSort)
- [Insertion Sort](https://gitlab.com/alandeqz/algorithms/-/tree/master/sorting/insertionSort)
- [Bubble Sort](https://gitlab.com/alandeqz/algorithms/-/tree/master/sorting/bubbleSort)